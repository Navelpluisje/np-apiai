/**
                         _      ___   _____         _
  _ __   _ __           /_\    / _ \  \_   \  __ _ (_)
 | '_ \ | '_ \  _____  //_\\  / /_)/   / /\/ / _` || |
 | | | || |_) ||_____|/  _  \/ ___/ /\/ /_  | (_| || |
 |_| |_|| .__/        \_/ \_/\/     \____/   \__,_||_|
        |_|
	Version: 1.0.0
	Author: Erwin Goossen
	Website: http://navelpluisje.nl
	Docs: https://bitbucket.org/Navelpluisje/np-APIai
	Repo: https://bitbucket.org/Navelpluisje/np-APIai
	Issues: https://bitbucket.org/Navelpluisje/np-APIai/issues
*/

const Installer = require('./installer/installer');

const installer = new Installer();
installer.start();