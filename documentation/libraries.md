# Libraries

## Cities ##

Provides all the cities, even the small ones, within the Netherlands. The list contains about 7400 cities, villages and communities.

The data has been subtracted from: [http://www.geonames.org/](http://www.geonames.org/)

## Colors ##

Colors has 2 entities:

1. The color names
1. Synonyms: the colors are grouped and will return one of the following values:
    * Rood
    * Oranje
    * Geel
    * Groen
    * Turquoise
    * Blauw
    * Paars
    * Roze
    * Bruin
    * Grijs
    * Wit

The data has been subtracted from: [http://helderester.nl/kleurentabel.html](http://helderester.nl/kleurentabel.html)

## Car brands ##

A list of almost 70 mostly popular car brands. Some of the already have synonyms for better matching. Because it are brands they are language independent.