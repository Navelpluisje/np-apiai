# Entities

## np-duration, np-age

These are almost the same: `np-age = np-duration + 'oud | jong'`
Check the units documentation for the different time units it matches

### Examples

| Input | Result |
|---|---|
| 2 dagen | `{"unit":"day","amount":2}` |
| 1 dag en 4 uur | `{"unit":["day","hour"],"amount":[1,4]}` |
| 1 uur, 2 min en 5 seconden | `	{"unit":["hour","min","sec"],"amount":[1,2,5]}` |

## np-length

Check the units documentation for the different length units it matches

### Examples

| Input | Result |
|---|---|
| 2 meter | `{"unit":"m","amount":2}` |
| 1 km | `{"unit":"km","amount":1}` |

## np-price

Check the units documentation for the different price units it matches
Np-price makes use of np-number which makes it possible to use a comma as decimal seperator. For that reason it's output is a string.
> *note:* Needs a way to handle thousand separator

### Examples

| Input | Result |
|---|---|
| € 2,50 | `{"amount":"2,50","unit":"EUR"}` |
| 50 euro | `{"unit":"EUR","amount":"50"}` |

## np-temperature

Check the units documentation for the different temperature units it matches

### Examples

| Input | Result |
|---|---|
| 10 °C | `{"unit":"C","amount":10}` |
| 50 Kelvin | `	{"unit":"K","amount":50}` |

## np-volume

Check the units documentation for the different volume units it matches

### Examples

| Input | Result |
|---|---|
| 10 liter| `{"unit":"l","amount":10}` |
| 25 cc | `	{"unit":"cl","amount":25}` |

## np-weight

Check the units documentation for the different weight units it matches

### Examples

| Input | Result |
|---|---|
| 10 kilogram| `{"unit":"kg","amount":10}` |

