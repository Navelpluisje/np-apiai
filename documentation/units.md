# Units

Units holds entities with different kinds of units.
All the return values are in english. This way it is better interchangeble with excisting entities.

> The list wil never be complete, but you can help make it as complete as possible

## unit-np-currency

Contains synonyms for different currencies

| Unit | Synonyms |
|------|----------|
| EUR | EUR, €, euro |
| USD | USD, $, dollar, us dollar |
| GBP | GBP, pound, pond |
| unknown | * |

## unit-np-length

Contains synonyms for different length units

| Unit | Synonyms |
|------|----------|
| km | km, kilometer |
| m | m, meter |
| cm | cm, centimeter |
| dm | dm, decimeter |
| hm | hm, hectometer |
| mm | mm, milimeter |
| unknown | * |

## unit-np-termperature

Contains synonyms for different temperature units

| Unit | Synonyms |
|------|----------|
| C | C, °C, °Celcius, ° Celcius, graden, graden celcius |
| F | F, °F, °Fahrenheit, ° Fahrenheit, graden Fahrenheit |
| K | K, Kelvin |
| R | R, Rankine |
| unknown | * |

## unit-np-time

Contains synonyms for different time units

| Unit | Synonyms |
|------|----------|
| sec | sec, seconde, seconden |
| min | min, minuut, minuten |
| hour | uur, uren |
| day | dag, dagen |
| week | week, weken |
| month | maand, maanden |
| quarter | kwartaal, kwartalen |
| year | jaar, jaren |
| decade | decennium, decennia |
| century | eeuw, eeuwen |
| millenium | millenium, millenia |
| unknown | * |

## unit-np-volume

Contains synonyms for different time units

| Unit | Synonyms |
|------|----------|
| ml | ml, mililiter |
| cl | cl, cc, centiliter |
| dl | dl, deciliter |
| l | l, liter |
| hl | hl, hectoliter |
| unknown | * |

## unit-np-weight

Contains synonyms for different weight units

| Unit | Synonyms |
|------|----------|
| mg | mg, miligram |
| g | g, gram |
| kg | kg, kilo, kilogram |
| unknown | * |
