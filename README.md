# API.ai Entities #

This repo contains some sets of dutch entities to use wit API.ai or others.
Feel free to extend them and create Pull Requests, or leave a request. For better usages, even outside API.ai, all entities are JSON formatted

Let me know if you like it:

[![Twitter](https://img.shields.io/twitter/url/http/shields.io.svg?style=social)](https://twitter.com/navelpluisje_nl)

---
## Usage ##

np-APIai can be used in 2 ways:

### 1. Use the installer

This way you can install selected entities automaticly. The only thing you need is your developers key. 
The installer is keeping track of dependecies and installs them as well. You will get no errors in API.ai of missing stuff.

* Clone or download this repository
* Go to the project in the terminal
* Run `npm install` to install all the dependecies
* Run `npm run np-apiai` and the installer starts

### 2. Copy manually

* Create a new entity in API.ai and give it a nice name
* Select `Switch to RAW mode`, this will open the CSV / JSON editor
* Select the JSON-tab
* Copy the content of the entity to the editor
* Click the `Save` button and your done

---
## Available entities ##

* [Entities](documentation/entities.md)
* [Libraries](documentation/libraries.md)
* [Units](documentation/units.md)

