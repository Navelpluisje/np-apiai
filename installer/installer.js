const chalk = require('chalk');
const clear = require('clear');
const http = require('https');
const figlet = require('figlet');
const fs = require('fs');
const questions = require('./questions');

class Installer {
    __constructor() {
        this.folder = '';
        this.files = [];
        this.token = '';
        this.post_options = {};
    }

	/**
	 * Start the installation process
	 */
    start() {
		// Set the API.ai url and path
        this.apiUrl = 'api.api.ai';
        this.apiPath = '/v1/entities?v=20150910';

		// Clear the console
        clear();

		// Show th header in ascii-art
        console.log(
         chalk.yellow(
            figlet.textSync('np-APIai', { 
                horizontalLayout: 'full',
                font: 'Ogre'
            })
            )
         );

		// And on to the first question
        questions.question_1(this.setFolder.bind(this));
    }

	/**
	 * Set the folder of entities
	 *
	 * @param {String} folder Name of the folder to 'open'
	 */
    setFolder(folder) {
        this.folder = folder;

		// Question 2, here we come
        questions.question_2(folder, this.setFiles.bind(this));
    }

	/**
	 * Set the list of entities to install
	 *
	 * @param {Array} files List of entity-names to install
	 */
    setFiles(files) {
        this.files = files;
        this.createFileList();
        this.buildDependencies(this.fileList.slice());

        console.log(`\n  A total of ${this.fileList.length} entities will be installed\n`);
        questions.question_3(this.setToken.bind(this));
    }

    setToken(token) {
        this.token = token;

		// Now we have all the data we want for posting.
		// Create the post oprions
        this.post_options = {
            host: this.apiUrl,
            path: this.apiPath,
            port: 443,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': `Bearer ${this.token}`
            }
        };

        questions.question_4(this.addEntities.bind(this));
    }

    addEntities(install) {
        if (!install) {
            console.log(chalk.red('  💥  💥  Installation aborted!!!'));
            return false;
        }

        console.log(chalk.green(' 🚀  Installation started!!'));
		// this.fileList.forEach(this.postEntity, this);
        this.postEntity(0);
    }

    postEntity(key) {
        const fileName = this.fileList[key];
        console.log(chalk.cyan(`    📦  installing ${chalk.bold(fileName)} to the API.ai`));		

        const entity = fs.readFileSync(fileName);
        const post_req = http.request(this.post_options, this._handleRequest.bind(this, key));

        post_req.on('error', function(e) {
            console.log('problem with request: ' + e.message);
        });

        post_req.write(entity);
        post_req.end();
    }

    _handleRequest(key, res) {
        res.setEncoding('utf8');
        res.on('data', this._handleRequestData.bind(this, key));		
    }

    _handleRequestData(key, chunk) {
        const result = JSON.parse(chunk);
        switch(result.status.code) {
        case 200:
            console.log(chalk.green(`      🎉  ${chalk.bold(this.fileList[key])} installed successfully`));
            break;
        case 409:
            console.log(chalk.yellow(`      🎭  ${chalk.bold(this.fileList[key])} has already been installed`));
        }

        if (this.fileList.length > key + 1) {
            this.postEntity(key + 1);		
        } else {
            this.stopInstallation(false);
        }
    }

    stopInstallation(error) {
        if (!error) {
            console.log('\n  🎂 🎂  Installation has succeeded successfully\n');		
        } else {
            console.log(chalk.red('\n  💥 💥  Installation ended with an error:\n'));
            console.log(chalk.red(error));		    		
        }
    }

	/**
	 * Create a list with the correct filenames and paths
	 */
    createFileList() {
        this.fileList = [];
        if (this.files.length === 0) {
            return false;
        }

        this.files.forEach((file) => {
            this.fileList.push(`./entity/${this.folder}/${file}`);
        });
    }

	/**
	 * Create the dependency list
	 *
	 * @param  {Array} fileList List of files to check on dependencies
	 *
	 * @return {Array}          Full list containg the filwList with all the dependencies
	 */
    buildDependencies(fileList) {

        fileList.forEach((fileName) => {
            console.log(chalk.green(`\n 🔎  Looking for dependencies for ${chalk.bold(fileName)}`));
            let dependencies = [];

			// Get the content of the entity
            let content = fs.readFileSync(fileName);
            let entity = JSON.parse(content);

			// Check if the entity has dependencies. 
			// If not go to the next one
            if (entity.dependencies.length === 0) {
                console.log(chalk.yellow(`   🚫  ${chalk.bold(fileName)} has no dependencies`));		
                return;
            }

            console.log(chalk.yellow(`   📌  ${chalk.bold(fileName)} has ${entity.dependencies.length} dependencies`));	

			// Loop through the dependencies and check if they already are in our list
			// If not we're going to add it	
            entity.dependencies.forEach((dependency) => {
                dependency = `./entity/${dependency}.json`;
                if (dependencies.indexOf(dependency) === -1 && this.fileList.indexOf(dependency) === -1) {
                    console.log(chalk.cyan(`     📦  ${chalk.bold(dependency)} has been added`));		
                    dependencies.unshift(dependency);
                }
            });

			// Check if our dependecies have dependencies themselves
            dependencies = this.buildDependencies(dependencies);

            dependencies.forEach((dependency) => {
                const index = this.fileList.indexOf(dependency);

                if (index === -1) {
                    this.fileList.unshift(dependency);
                }
            });
        });

        return fileList;
    }
}

module.exports = Installer;