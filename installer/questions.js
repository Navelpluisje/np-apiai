const inquirer = require('inquirer');
const fs = require('fs');

module.exports = {
    question_1: (callback) => {
        const files = fs.readdirSync('./entity');

        inquirer.prompt({
            type: 'rawlist',
            name: 'folder',
            choices: files,
            message: 'What type of entity do you want to add?',
            validate: function( value ) {
                if (value.length) {
                    return true;
                } else {
                    return 'Please select a type';
                }
            }		
        }).then((folder) => {
            callback(folder.folder);
            return;
        });
    },

    question_2: (group, callback) => {
        const files = fs.readdirSync(`./entity/${group}`);

        inquirer.prompt({
            type: 'checkbox',
            name: 'group',
            choices: files,
            message: `Which entities of the type ${group} do you want to add?`,
            validate: function( value ) {
                if (value.length) {
                    return true;
                } else {
                    return 'Please select at least one entity';
                }
            }		
        }).then((group) => {
            callback(group.group);
            return;
        });
    },

    question_3: (callback) => {
        inquirer.prompt({
            type: 'text',
            name: 'token',
            message: 'Your API.ai developer token:',
            validate: function( value ) {
                if (value.length) {
                    return true;
                } else {
                    return 'Please enter a name for the repository';
                }
            }		
        }).then((token) => {
            callback(token.token);
            return;
        });
    },

    question_4: (callback) => {
        inquirer.prompt({
            type: 'confirm',
            name: 'install',
            message: 'Install the entities',
            default: true
        }).then((install) => {
            callback(install.install);
            return;
        });
    }
};